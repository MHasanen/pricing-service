package com.udacity.pricing.api;

import com.udacity.pricing.domain.price.Price;
import org.springframework.data.repository.CrudRepository;


public interface PricingController extends CrudRepository<Price, Long> {

}
